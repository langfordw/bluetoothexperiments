import serial
import time
import random

ser = serial.Serial('/dev/cu.usbserial-FTF4ZHMI', 115200);

ser.write('$$$'.encode('ascii'));

alpha = 0.1
value = 120;

while(1):
	value = round(alpha*(random.random()*100+100) + (1-alpha)*value);
	# print('SHW,0072,00a1\r'.encode('ascii'))
	# ser.write('SHW,0072,00a1\r'.encode('ascii'));
	hexstring = hex(value)[2:]
	string = "SHW,0072,00"+hexstring+"\r"
	print(string.encode('ascii'));
	ser.write(string.encode('ascii'));
	time.sleep(0.25)