## Experimenting with the RN4871



I made Neil's [simple FTDI board](http://academy.cba.mit.edu/classes/networking_communications/RN4871/hello.RN4871.ftdi.png) described on [this page](http://academy.cba.mit.edu/classes/networking_communications/index.html).

After reflowing the solder a few times, I got it to communicate with the Arduino serial monitor.

The command set is described in detail in [Microchip's User Guide](https://www.mouser.com/pdfdocs/RN4870-1UsersGuide.pdf).

By default the serial baudrate should be set to 115200 and a carriage return is used to delimit commands.

> EXCEPT! The initial `$$$` (which is used to put the module into command mode) must be sent with no carriage return.

So in the serial monitor send the following:

```
$$$
+				// turn echo on
SS,C0			// default service configuration (device info + UART transport...)
SN,myRN4871		// set name
R,1				// reboot
```

Sending the first `$$$` should be followed with a `CMD>` response if all went well. After each command we should receive a `AOK` response if it was successful or an `Err` response if it failed.

With those commands (and the module still plugged into the computer), I could now use the microchip smart sensor BLE app on my iPhone and connect with the module.

This is enough functionality to establish a UART bridge over bluetooth such that commands sent from the serial terminal program can be seen on the phone.

This module also lets you act as other standard bluetooth devices. For example, I got it to show up as a heart-rate monitor service. I found [this page](http://microchipdeveloper.com/ble:rn4870-app-example-updating-server-characteristics) very helpful for getting the bluetooth service functionality working. The following commands enable the heart rate monitor service and turn on three of the service "characteristics": heart-rate measurement, body sensor location, and heart rate control point. 

```
CMD> PS,180D
AOK
CMD> PC,2A37,10,05
AOK
CMD> PC,2A38,02,05
AOK
CMD> PC,2A39,08,05
AOK
CMD> R,1
```

The details of these services and their corresponding characteristics and handles are described in the [official bluetooth service documentation](https://www.bluetooth.com/specifications/gatt/services).

I wanted to see if I could read the heart-rate measurements in my browser with Javascript. I first tried [this example](https://github.com/WebBluetoothCG/demos/tree/gh-pages/heart-rate-sensor) but found that my device wasn't showing up for some reason. I think I'm still not configuring something correctly because I also cannot see the device from the system preferences on my Mac or my iPhone. However, with a small modification I was able to get this to work. I simply replaced the `filters:[{services:[ 'heart_rate' ]`  parameter of the `requestDevice()` function call with `acceptAllDevices':true,'optionalServices': [0x180D]`. Note that the optionalServices field appears to be required when accepting all devices and that the 0x180D is that of the "heart-rate service" and is what I entered above with `PS,180D`.

Now, I was able to connect to the sensor but I wasn't sending or receiving any data. To do this, I wrote a quick python script to feed random heart-rate values over the serialport:

```python
import serial
import time
import random

alpha = 0.1
value = 120;

# open serial port
ser = serial.Serial('/dev/cu.usbserial-FTF4ZHMI', 115200);

# enter command mode
ser.write('$$$'.encode('ascii'));

while(1):
    # exponentially weighted moving average random heart rate
	value = round(alpha*(random.random()*100+100) + (1-alpha)*value);	
    
	hexstring = hex(value)[2:];				# strip \x
	string = "SHW,0072,00"+hexstring+"\r";	# combine into string
    
    # convert to ascii bytes and send over serial
	print(string.encode('ascii'));
	ser.write(string.encode('ascii'));
    
    # delay
	time.sleep(0.25);
```

And the result is this:

![demo.mov](video/demo.mov)

The module also has an onboard [scripting language](http://ww1.microchip.com/downloads/en/DeviceDoc/50002466B.pdf) that looks just powerful enough to be useful for very minimal applications.





